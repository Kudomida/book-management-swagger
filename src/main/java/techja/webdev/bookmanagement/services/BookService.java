package techja.webdev.bookmanagement.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import techja.webdev.bookmanagement.models.Book;
import techja.webdev.bookmanagement.repositories.BookRepository;

import java.util.List;

public interface BookService {
	
	/**
	 * Get list of book in Database.
	 *
	 * @return list of book.
	 */
	public List<Book> getBookList();
	
	/**
	 * Add content for message.
	 *
	 * @return message with content.
	 */
	public String getMessage();
	
	/**
	 * Handle create new book action.
	 *
	 * @param name				name of book
	 * @param author			author of book
	 * @param price				price of book
	 * @param publishingYear	publishing year
	 */
	public void createNewBook(String name, String author, String price, String publishingYear);
	
	/**
	 * Handle find book by id.
	 *
	 * @param id	id of book
	 * @return		book found
	 */
	public Book findBookById(String id);
	
	/**
	 * Handle update book action.
	 *
	 * @param id				id of book
	 * @param name				name of book
	 * @param author			author of book
	 * @param price				price of book
	 * @param publishingYear	publishing year
	 */
	public void updateBook(String id, String name, String author, String price, String publishingYear);
	
	/**
	 * Handle delete book action by id.
	 *
	 * @param id	id of book.
	 */
	public void deleteBook(String id);
	
}
