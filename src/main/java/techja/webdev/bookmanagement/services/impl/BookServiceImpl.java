package techja.webdev.bookmanagement.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import techja.webdev.bookmanagement.models.Book;
import techja.webdev.bookmanagement.repositories.BookRepository;
import techja.webdev.bookmanagement.services.BookService;

import java.util.List;

@SuppressWarnings("SpellCheckingInspection")
@Service

public class BookServiceImpl implements BookService {
	
	@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
	@Autowired
	BookRepository bookRepository;
	
	
	/**
	 * Get list of book in Database.
	 *
	 * @return list of book.
	 */
	@Override
	public List<Book> getBookList() {
		return bookRepository.findAll();
	}
	
	/**
	 * Add content for message.
	 *
	 * @return message with content.
	 */
	@Override
	public String getMessage() {
		String msg = "";
		if (getBookList().size() == 0) {
			msg = "Không có Book để hiển thị!";
		}
		return msg;
	}
	
	/**
	 * Handle create new book action.
	 *
	 * @param name           name of book
	 * @param author         author of book
	 * @param price          price of book
	 * @param publishingYear publishing year
	 */
	@Override
	public void createNewBook(String name, String author, String price, String publishingYear) {
		Book book = new Book();
		book.setName(name);
		book.setAuthor(author);
		book.setPrice(price);
		book.setPublishingYear(publishingYear);
		bookRepository.save(book);
		System.out.println("Create new book success!");
	}
	
	/**
	 * Handle find book by id.
	 *
	 * @param id id of book
	 * @return book found
	 */
	@Override
	public Book findBookById(String id) {
		return bookRepository.findBookById(Integer.parseInt(id));
	}
	
	/**
	 * Handle update book action.
	 *
	 * @param id             id of book
	 * @param name           name of book
	 * @param author         author of book
	 * @param price          price of book
	 * @param publishingYear publishing year
	 */
	@Override
	public void updateBook(String id, String name, String author, String price, String publishingYear) {
		Book book = bookRepository.findBookById(Integer.parseInt(id));
		book.setName(name);
		book.setAuthor(author);
		book.setPrice(price);
		book.setPublishingYear(publishingYear);
		bookRepository.save(book);
		System.out.println("Update book success!");
	}
	
	/**
	 * Handle delete book action by id.
	 *
	 * @param id id of book.
	 */
	@Override
	public void deleteBook(String id) {
		String[] idArr = id.split(",");
		for (String s : idArr) {
			bookRepository.delete(bookRepository.findBookById(Integer.parseInt(s)));
		}
		System.out.println("Delete book success!");
	}
}
