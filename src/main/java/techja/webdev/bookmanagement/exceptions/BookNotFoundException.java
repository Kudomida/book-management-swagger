package techja.webdev.bookmanagement.exceptions;

public class BookNotFoundException extends RuntimeException{
    public BookNotFoundException(Long id){
        super("Không tìm thấy Book có id: " + id);
    }
}
