package techja.webdev.bookmanagement.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ViewImageController {

	@RequestMapping(value = "/viewImage", method = RequestMethod.GET)
	public String viewImage(){

		return "ViewImage";
	}
}
