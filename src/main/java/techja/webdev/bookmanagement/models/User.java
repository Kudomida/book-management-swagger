package techja.webdev.bookmanagement.models;

import lombok.Generated;

import javax.persistence.Id;

public class User {
    @Id
    @Generated
    private Long id;
    private String name;
    private String address;
}
